! https://app.codesignal.com/arcade/intro/level-1/egbueTZRRL5Mm4TXN
! 
! To build:
!  gfortran centuryFromYear.f90 -o centuryFromYear
!
! To run:
!  ./centuryFromYear

function getCentury(year) result(century)
    implicit none

    ! Initialize the needed variables
    integer :: year
    integer :: adjustedYear
    integer :: century

    ! Subtract one year so that 2000 is 1999, since it is
    ! the 19th century
    adjustedYear = year - 1
    adjustedYear = adjustedYear + 100

    ! Divide year by 100 to get century
    century = adjustedYear / 100
end function getCentury

program centuryFromYear
    implicit none

    ! Initalize function
    integer :: getCentury
    integer :: year
    integer :: century

    ! Get year from user
    print *, 'Enter a four digit year -'
    read (*,*) year

    ! Print output
    century = getCentury(year)
    print *, 'The century is: ', century
end program
    