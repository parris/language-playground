! Given an array of integers, find the pair of adjacent elements that has the largest product and return that product.
! https://app.codesignal.com/arcade/intro/level-2/xzKiBHjhoinnpdh6m/solutions

! TIL: Fortran functions cannot have print statements or else they silently freeze

program adjacentElementsProduct
    implicit none
    
    ! This type comes from the coding challenge
    type ArrayInteger 
        integer, allocatable, dimension(:) :: arr
    end type ArrayInteger

    ! Declare input array
    type(ArrayInteger) :: inputArray
    type(ArrayInteger) :: inputArray2
    type(ArrayInteger) :: inputArray3
    ! integer :: adjacentElementsProduct

    ! Initialize some arrays
    allocate(inputArray%arr(10))
    inputArray%arr = [0,5,3,24,5,8,7,18,9,10]

    allocate(inputArray2%arr(8))
    inputArray2%arr = [-3,5,-2,-10,-4,23,-1,1]

    allocate(inputArray3%arr(3))
    inputArray3%arr = [-3,0,99]

    ! Call the function
    print *, "Calling function with:", inputArray%arr
    print *, "Result:", findHighestProductPair(inputArray)

    ! Call the function
    print *, "Calling function with:", inputArray2%arr
    print *, "Result:", findHighestProductPair(inputArray2)

    ! Call the function
    print *, "Calling function with:", inputArray3%arr
    print *, "Result:", findHighestProductPair(inputArray3)
contains
    function findHighestProductPair(inputArray) result(res)
        type(ArrayInteger) :: inputArray
        integer(4) :: res
        integer :: i
        integer :: maxPair
        integer :: pairSize
        integer :: resetMaxPair = 0
        
        ! Loop over array
        do i = 1, size(inputArray%arr) -1
            pairSize = inputArray%arr(i) * inputArray%arr(i+1)

            if (resetMaxPair == 1) then
                maxPair = pairSize
                resetMaxPair = 0
            end if
            
            if (pairSize > maxPair) then
                maxPair = pairSize
            end if
            
        end do
        
        resetMaxPair = 1
        res = maxPair
    end function findHighestProductPair
end program