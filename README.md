# Language Playground

It's time to play with the unpopular languages.

Inspired by [http://paulgraham.com/weird.html](http://paulgraham.com/weird.html)

Pull challenges from various places like leetcode and codesignal, solve them in unfamiliar languages

## Fortran

Install compiler on Mac:

```bash
brew install gcc
```

To build:

```bash
# For example
gfortran adjacentElementsProduct.f90 -o .compiled/adjacentElementsProduct
```

To run:

```bash
./.compiled/adjacentElementsProduct 
```